<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class C extends Controller
{
    public function index()
    {
        return view('\screen\dashboard', [
        "title" => "Dashboard",
        "class" => "dashboard"
    ]);
    }//
}
