@extends('layouts.main')

@section('content')

<div class="main-content">
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header">
            <div class="icon icon-warning">
              <span class="material-icons">person</span>
            </div>
          </div>
          <div class="card-content">
            <p class="category"><strong>Anggota</strong></p>
            <br />
            <br />
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons text-info">info</i>
              <a href="mahasiswa">Lihat lebih banyak</a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header">
            <div class="icon icon-warning">
              <span class="material-icons">book</span>
            </div>
          </div>
          <div class="card-content">
            <p class="category"><strong>Buku</strong></p>
            <br />
            <br />
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons text-info">info</i>
              <a href="#pablo">Lihat lebih banyak</a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header">
            <div class="icon icon-warning">
              <span class="material-icons">equalizer</span>
            </div>
          </div>
          <div class="card-content">
            <p class="category"><strong>Peminjaman</strong></p>
            <br />
            <br />
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons text-info">info</i>
              <a href="#pablo">Lihat lebih banyak</a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header">
            <div class="icon icon-warning">
              <span class="material-icons">equalizer</span>
            </div>
          </div>
          <div class="card-content">
            <p class="category"><strong>Pengembalian</strong></p>
            <br />
            <br />
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons text-info">info</i>
              <a href="#pablo">Lihat lebih banyak</a>
            </div>
          </div>
        </div>
      </div>
@endsection