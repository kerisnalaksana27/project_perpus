    <!-- Sidebar  -->
    <nav id="sidebar">
      <div class="sidebar-header">
        <h3><img src="img/buuk.jpg" class="img-fluid" /><span>Perpus</span></h3>
      </div>
      <ul class="list-unstyled components">
        <li class="active">
          <a href="#" class="dashboard"><i class="material-icons">dashboard</i><span>Dashboard</span></a>
        </li>

        <div class="small-screen navbar-display">
          <li class="dropdown d-lg-none d-md-block d-xl-none d-sm-block">
            <a href="#homeSubmenu0" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> <i class="material-icons">notifications</i><span> 4 notification</span></a>
            <ul class="collapse list-unstyled menu" id="homeSubmenu0">
              <li>
                <a href="#">You have 5 new messages</a>
              </li>
              <li>
                <a href="#">You're now friend with Mike</a>
              </li>
              <li>
                <a href="#">Wish Mary on her birthday!</a>
              </li>
              <li>
                <a href="#">5 warnings in Server Console</a>
              </li>
            </ul>
          </li>

          <li class="d-lg-none d-md-block d-xl-none d-sm-block">
            <a href="#"><i class="material-icons">apps</i><span>apps</span></a>
          </li>

          <li class="d-lg-none d-md-block d-xl-none d-sm-block">
            <a href="#"><i class="material-icons">person</i><span>user</span></a>
          </li>

          <li class="d-lg-none d-md-block d-xl-none d-sm-block">
            <a href="#"><i class="material-icons">settings</i><span>setting</span></a>
          </li>
        </div>

        <li class="dropdown">
          <a href="#pageSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> <i class="material-icons">aspect_ratio</i><span>Kelola Data</span></a>
          <ul class="collapse list-unstyled menu" id="pageSubmenu1">
            <li>
              <a href="#">Anggota</a>
            </li>
            <li>
              <a href="#">Buku</a>
            </li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> <i class="material-icons">apps</i><span>Transaksi</span></a>
          <ul class="collapse list-unstyled menu" id="pageSubmenu2">
            <li>
              <a href="#">Peminjaman</a>
            </li>
            <li>
              <a href="#">Pengembalian</a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>

    <!-- Page Content  -->
    <div id="content">
      <div class="top-navbar">
        <nav class="navbar navbar-expand-lg">
          <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="d-xl-block d-lg-block d-md-mone d-none">
              <span class="material-icons">home</span>
            </button>

            <a class="navbar-brand" href="#"> Dashboard </a>

            <button
              class="d-inline-block d-lg-none ml-auto more-button"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="material-icons">more_vert</span>
            </button>

            <div class="collapse navbar-collapse d-lg-block d-xl-block d-sm-none d-md-none d-none" id="navbarSupportedContent">
              <ul class="nav navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <span class="material-icons">apps</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <span class="material-icons">person</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <span class="material-icons">settings</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
